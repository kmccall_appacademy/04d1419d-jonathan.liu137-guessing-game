def guessing_game
  target = 1 + rand(100)
  tries = 0
  guess = 0

  while guess != target
    puts "guess a number"
    guess = gets.chomp.to_i

    puts "#{guess} is too high" if guess > target
    puts "#{guess} is too low" if guess < target
    tries += 1
  end

  puts "Congrats, you guessed the number (#{target})!"
  puts "It took you #{tries} guesses."
end

def file_shuffler
  print "Enter a file name: "
  file_name = gets.chomp
  shuffled = File.readlines(file_name).shuffle
  File.open("#{file_name}-shuffled.txt", "w") do |f|
    f.puts shuffled
  end
end
